import socket
import sys


class Port_scan:
    def __init__(self, target):
        self.ports = [20, 21, 22, 23, 25, 80, 113, 443, 1433, 1434, 3306, 5432, 8080]
        self.target = target

    def init_scan(self):
        for port in self.ports:
            client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            client.settimeout(0.1)
            code = client.connect_ex((self.target, port))

            if code == 0: print(f"{self.target}--> {port}: OPEN")


argument = sys.argv
target = argument[1]

scan = Port_scan(target)
scan.init_scan()
